<?php
function tentukan_nilai($number)
{
    if($number >= 85){
    	echo "Nilai anda: ". $number ." dengan Predikat: Sangat Baik<br>";
    }elseif($number >= 70){
		echo "Nilai anda: ". $number ." dengan Predikat: Baik<br>";
    }elseif ($number >= 60) {
    	echo "Nilai anda: ". $number ." dengan Predikat: Cukup<br>";
    }else{
    	echo "Nilai anda: ". $number ." dengan Predikat: Kurang<br>";
    }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>