<?php
function ubah_huruf($string){
	$huruf = "abcdefghijklmnopqrstuvwxyz";
	$wadah = "";

	for ($a = 0; $a < strlen($string); $a++){
		$position = strrpos($huruf, $string[$a]);
		$wadah .= substr($huruf, $position +1,1);
	}
	echo "<br>";
	return $wadah;
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>